import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import * as actions from "./state/actions/actions";
import { user } from "./state/mapStates/map_states";

import { auth } from "./firebase/firebase";

import Header from "./containers/Header";
import FooterContainer from "./containers/Footer";

// Componentes de rotas sempre disponíveis
import HomeScreen from "./pages/HomeScreen";
import ErrorPage from "./pages/ErrorPage";
import { LoadingScreen } from "./containers/LoadinScreen";
import AdicionaisParaPartes from "./pages/AdicionaisParaPartes";

// Os componentes de rotas abaixo são importados apenas quando são necessários
// tornando o aplicativo menor e mais rápido

import {
  MenuScreen,
  ProductsListScreen,
  ProductScreen,
  PedidoScreen,
  CarrinhoScreen,
  PartsScreen,
  AdressScreen,
  Pagamento,
  ResumeScreen,
  UserScreen,
  EditarEndereco,
  InformacoesScreen,
  LoginScreen,
  PromocoesScreen,
  ResetarSenha
} from "./pages/asyncComponents";

class App extends Component {
  state = {
    user: false,
    admin: false,
    showLoadingScreen: true
  };

  componentDidMount() {
    // Apagar em produção
    localStorage.clear();
    // verifica se o usuário está logado e toma as ações necessárias.
    this.props.checkForUser();
    // busca os produtos na base de dados
    this.props.fetchProducts();
    // atualiza o horário de atendimento
    this.props.updateHorario();

    this.props.getOrdersNumber();

    this.props.getPromOProducts();

    auth.onAuthStateChanged(user => {
      this.setState({ showLoadingScreen: true });

      window.setTimeout(_ => this.setState({ showLoadingScreen: false }), 2000);
      if (user) {
        window.setTimeout(_ => {
          this.setState({ showLoadingScreen: false }, _ => {});
        }, 1000);
      }
    });
  }

  render() {
    // Cada tela do programa tem sua própria rota,
    // nos componentes <Route/>, verificar mais abaixo,
    // a variavel "path" determina o caminho e parametros da url
    // e a varável "component" informa qual componente renderizar
    return (
      <BrowserRouter>
        <Fragment>
          <LoadingScreen show={this.state.showLoadingScreen} />
          <Header />
          <Switch>
            <Route exact path="/" component={HomeScreen} />
            <Route path="/menu" component={MenuScreen} />
            <Route
              path="/produto/:produto/:tamanho/:tipo"
              component={ProductsListScreen}
            />
            <Route path="/detalhe/:produto/:id" component={ProductScreen} />
            <Route path="/pedidos/" component={PedidoScreen} />
            <Route path="/carrinho/" component={CarrinhoScreen} />
            <Route path="/partes/" component={PartsScreen} />
            <Route path="/confirmar/" component={AdressScreen} />
            <Route path="/pagamento/" component={Pagamento} />
            <Route path="/resumo/" component={ResumeScreen} />
            <Route path="/conta/" component={UserScreen} />
            <Route path="/editar-endereco/" component={EditarEndereco} />
            <Route path="/informacoes/" component={InformacoesScreen} />
            <Route path="/login/" component={LoginScreen} />
            <Route path="/reset/" component={ResetarSenha} />
            <Route path="/promocoes/" component={PromocoesScreen} />
            <Route path="/teste/" component={AdicionaisParaPartes} />
            <Route component={ErrorPage} />
          </Switch>
          <FooterContainer />
        </Fragment>
      </BrowserRouter>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    newPrice: p => dispatch({ type: actions.NEW_UPDATED_PRICE, payload: p }),
    newOrder: p => dispatch({ type: actions.NEW_ORDER, payload: p }),
    removeOrder: p => dispatch({ type: actions.NEW_REMOVE_ORDER, payload: p }),
    fetchProducts: () =>
      dispatch({ type: actions.FETCH_PIZZAS_FROM_FIRESTORE }),
    checkForUser: () => dispatch({ type: actions.NEW_CHECK_FOR_USER }),
    updateHorario: () => dispatch({ type: actions.UPDATE_HORARIO }),
    getOrdersNumber: () => dispatch({ type: actions.GET_ORDER_WATCHER }),
    getPromOProducts: () => dispatch({ type: actions.WATCH_GET_PROMO_PRODUCTS })
  };
};

export default connect(
  user,
  mapDispatchToProps
)(App);
