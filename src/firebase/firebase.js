import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

firebase.initializeApp({
  apiKey: "AIzaSyCp9QzLbkbT6MUwU807kr_R2gf_Y3-Rssk",
  authDomain: "notas-365.firebaseapp.com",
  databaseURL: "https://notas-365.firebaseio.com",
  projectId: "notas-365",
  storageBucket: "notas-365.appspot.com",
  messagingSenderId: "740723294888"
});

const firestore = firebase.firestore();
firestore.settings({ timestampsInSnapshots: true });

export const database = firestore;
export const auth = firebase.auth();
export const signIn = _ => {
  const provider = new firebase.auth.GoogleAuthProvider();
  return firebase.auth().signInWithRedirect(provider);
};

export const signInWithEmail = ({ email, password }) => {
  auth
    .signInWithEmailAndPassword(email, password)
    .then(_ => (window.location.pathname = "/"));
};

export const createWithEmail = async ({ email, password, displayName }) => {
  await auth.createUserWithEmailAndPassword(email, password);
  window.location.pathname = "/";
};

export const signOut = _ => {
  firebase
    .auth()
    .signOut()
    .then(_ => {
      return (window.location.pathname = "/");
    });
};

export const deleteAccount = _ => {
  const response = window.confirm('Você deseja mesmo apagar sua conta? Isso não poderá ser desfeito!');

  if (response) {
    firebase.auth().currentUser.delete().then(res => {
      signOut()
    })
  }
};

