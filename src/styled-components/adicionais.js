import styled from 'styled-components';
import { color } from './colors';
import { typography } from './typography';

export const AdicionaisButton = styled.button`
    max-width: ${props => props.full ? '100%' : '120px'};
    width: 100%;
    display: flex;
    justify-content: ${props => props.center ? 'center' : 'space-between'};
    align-items: center;
    text-align: center;
    background: ${ props => props.filled ? color.backgroundGreenLight : 'transparent'};
    color: ${ color.primary};
    border: ${props => props.marked ? `2px solid ${color.secondary}` : '2px solid transparent'};
    border-radius: 5px;
    padding: 15px 10px 15px 5px;
    margin: ${ props => props.full ? '20px 0 0 0' : '0 0 10px'};
    cursor: pointer;
    font-size: 16px;
    font-weight: ${ props => props.filled ? '600' : '500'};
    font-family: ${typography.body};
`

export const AdicionaisLabel = styled.label`
    max-width: ${props => props.full ? '100%' : '120px'};
    width: 100%;
    display: flex;
    justify-content: ${props => props.center ? 'center' : 'space-between'};
    align-items: center;
    text-align: center;
    background: ${ color.backgroundGreenLight};
    color: ${ color.primary};
    border: ${props => props.marked ? `2px solid ${color.secondary}` : '2px solid transparent'};
    border-radius: 5px;
    padding: 15px 10px 15px 5px;
    margin: ${ props => props.full ? '10px 0 0 0' : '0 0 10px'};
    cursor: pointer;
    font-size: 16px;
    font-weight: ${ props => props.filled ? '600' : '500'};
    font-family: ${typography.body};
`

export const AdicionaisPrice = styled.div`
    color: ${ color.secondary};

`

export const AdicionaisInput = styled.input`
    opacity: 0;
    position: absolute;
`

