import styled from 'styled-components';
import { color } from './colors';

export const Divider = styled.div`
    width: 100%;
    height: 1px;
    background: ${color.primary};
    margin-bottom: 10px;
`
