import styled from "styled-components";
import { color } from './colors';
import { typography } from './typography';

export const HomeNavContainer = styled.nav`
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;
    max-width: 320px;
    width: 100%;
    margin: 50px auto 0;
`

export const SquaredButton = styled.button`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 90px;
    height: 80px;
    padding: 5px;
    margin: 5px;
    background: ${color.backgroundGreenLight};
    color: ${color.primary};
    font-family: ${typography.body};
    font-weight: 600;
    border: none;
    border-radius: 5px;
    font-size: 12px;
    cursor: pointer;

    & .icon {
        font-size: 28px;
        margin-bottom: 10px;
        color: ${color.primary};
    }
`
