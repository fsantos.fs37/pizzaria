import styled from 'styled-components';
import { color } from './colors';
import { typography } from './typography';

export const ProductButtonContainer = styled.button`
    position: relative;
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: stretch;
    background: ${color.backgroundGreenLight};
    border: none;
    border-radius: 5px;
    padding: 15px 10px;
    margin: 0 0 10px;
    cursor: pointer;
`

export const ProductButtonTitle = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
    width: 100%;
    font-size: ${props => props.small ? '16px' : '21px'};
    font-weight: 700;
    color: ${color.primary};
    font-family: ${typography.body};
    margin: 0 0 10px;
    text-align: left;   
`

export const ProductButtonDescription = styled.div`
    text-align: left;
    font-size: 14px;
    font-weight: 700;
    margin: 0;
    color: ${color.primary};
    font-family: ${typography.body};
`

export const ProductPriceTag = styled.p`
    font-size: 16px;
    font-weight: 700;
    margin: 0;
    padding: 2px 9px 0;
    border-radius: 3px;
    color: ${color.secondary};
    font-family: ${typography.body};
    right: 10px;
    top: 15px;
    font-family: 'Lekton', sans-serif;
`
