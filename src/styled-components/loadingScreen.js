import styled from 'styled-components';
import { color } from './colors';
import { typography } from './typography';

import backgroundImage from '../assets/carregando.png'

export const LoadingScreenContainer = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: #ffeb3b;
    background-image: url(${backgroundImage});
    background-size: cover;
    display: ${props => props.showLoadingScreen ? 'flex' : 'none'};
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    z-index: 9999;
`

export const Logo = styled.img`
    width: 100%;
    max-width: 200px;
    height: 174.313px;
    margin: 50px auto;
    border-radius: 10px
`

export const LoadingMessage = styled.h1`
    font-size: 28px
    color: ${color.light};
    font-family: ${typography.head};
    text-align: center;
`
