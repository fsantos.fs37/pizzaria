import styled from 'styled-components';
import { typography } from './typography';
import { color } from './colors';

export const AdressCardContainer = styled.div`
    width: 100%;
    padding: 1px 10px;
    margin-top: 30px;
    border-radius: 5px;
    background: ${color.backgroundGreenLight};
    color: rgba(0,0,0,.8);
    font-family: ${typography.body};
    font-size: 14px;
`

export const AdressCardTitle = styled.p`
    font-size: 17px;
    font-weight: 600;
`

export const AdressCardText = styled.p`
    
`
