import styled from 'styled-components';
import { color } from './colors';
import { typography } from './typography';

export const PartsLabel = styled.label`
    max-width: ${props => props.full ? '100%' : '120px'};
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: ${props => props.center ? 'center' : 'space-between'};
    text-align: center;
    background: ${ color.backgroundGreenLight};
    color: ${ color.primary};
    border: ${props => props.marked ? `2px solid ${color.secondary}` : '2px solid transparent'};
    border-radius: 5px;
    padding: 15px 10px 15px 5px;
    margin: ${ props => props.full ? '10px 0 0 0' : '0 0 10px'};
    cursor: pointer;
    font-size: 16px;
    font-weight: ${ props => props.filled ? '600' : '500'};
    font-family: ${typography.body};
`

export const PartsIngredients = styled.div`
    font-family: ${typography.body};
    color: ${color.primary};
    font-size: 14px;
    font-weight: 700
`

export const PartsInput = styled.input`
    opacity: 0;
    position: absolute;
`


