import styled, { keyframes } from 'styled-components';
import { color } from './colors';
import { typography } from './typography';

const fadeintoView = keyframes`
    to {
        visibility: visible;
        opacity: 1
    }
`

export const StyledSection = styled.section`
    width: 100%;
    min-height: calc(100vh - 50px);
    max-width: 600px;
    margin: 0 auto 0;
    padding: 70px 10px 100px;
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
    opacity: 0;
    animation-name: ${fadeintoView};
    animation-duration: .3s;
    animation-timing-function: ease-in-out;
    animation-fill-mode: forwards;
`

export const SectionTitle = styled.h1`
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
    width: 100%;
    background: transparent;
    top: 0;
    left: 0;
    z-index: 999;
    padding: 0;
    margin: 0 auto;
    font-size: 24px;
    font-weight: 400;
    font-family: ${typography.head};
    color: ${color.light};
    text-transform: capitalize
`

export const SectionText = styled.p`
    font-family: ${typography.body};
    font-size: 14px;
    max-width: 60ch;
    color: ${color.light};
`

export const Divider = styled.div`
    width: 100%;
    height: 1px;
    background: ${color.primary}
`
