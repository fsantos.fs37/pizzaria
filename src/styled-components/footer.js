import styled from "styled-components";
import { color } from './colors';
import { typography } from './typography';

export const StyledFooter = styled.footer`
    width: 100%;
    position: fixed;
    bottom: 0;
    transform: ${props => props.visible ? 'translateY(0)' : 'translateY(102%)'};
    transition: transform .3s ease-in-out;
    transition-delay: .3s;
`

export const InnerFooter = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    max-width: 780px;
    width: 95%;
    height: 70px;
    padding: 5px;
    margin: auto;
    border-radius: 5px 5px 0 0;
    background: ${color.backgroundGreenLight};
    box-shadow: 0 0 8px 0 rgba(0,0,0,.1);
    font-family: 'Montserrat', sans-serif;
    color: #fd5523;
`

export const FooterPriceButton = styled.button`
    border: none;
    border-radius: 5px;
    background: transparent;
    cursor: pointer;
    outline: none;
    font-weight: 500;
    font-size: 16px;
    color: ${color.secondary};
    font-family: ${typography.head};
    visibility: ${props => props.invisible ? 'hidden' : null}
`;

export const FooterOrderButton = styled.button`
    padding: 7px 0;
    margin: 0 6px;
    border: none;
    border-radius: 5px;
    background: transparent;
    color: ${color.primary};
    font-family: ${typography.body};
    font-size:  16px;
    font-weight: 600;
    cursor: pointer;
    outline: none;
`


