import styled from 'styled-components';
import { color } from './colors';
import { typography } from './typography';

export const PartOptionContainer = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 100%;
`
export const PartOptionInput = styled.input`
    opacity: 0;
    position: absolute;
    pointer-events: none;
`
export const PartOptionLabel = styled.label`
    font-family: 'Lekton', sans-serif;
    font-size: 14px;
    font-weight: 600;
    color: rgba(0,0,0,.8);
    text-align: center;
    padding: 1px 5px;
    border-radius: 5px;
    cursor: pointer;
    border: ${props => props.checked ? '2px solid #fd5523' : '2px solid transparent'}
`

export const PartesButton = styled.button`
    max-width: ${props => props.full ? '100%' : '120px'};
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
    background: ${ color.backgroundGreenLight};
    color: ${ color.primary};
    border: none;
    border-radius: 5px;
    padding: 14px 10px 14px 5px;
    margin: ${ props => props.full ? '10px 0 0 0' : '0 0 10px'};
    cursor: pointer;
    font-size: 14px;
    font-weight: ${ props => props.filled ? '600' : '500'};
    font-family: ${typography.body};

`
