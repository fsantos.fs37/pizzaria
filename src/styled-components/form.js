import styled from 'styled-components';
import { color } from './colors';
import { typography } from './typography';

export const Form = styled.form`
    width: 100%;
    max-width: 500px;
    margin: 20px 0;
    display: flex;
    flex-direction: column;
    font-family: ${typography.body};
    color: rgba(0,0,0,.8);
    background: #ffffff;
    padding: 0 10px;
    border-radius: 10px;

    input[type="password"] {
        font:small-caption;font-size:16px;
    }

`

export const FormTitle = styled.p`
    font-family: ${typography.body};
    color: rgba(0,0,0,.8);
    font-size: 24px;
`

export const Label = styled.label`
    display: flex;
    flex-direction: column;
    width: 100%;
    font-size: 17px;
`

export const Input = styled.input`
    width: 100%;
    padding: 15px 5px;
    border: none;
    border-radius: 5px;
    border: 1px solid rgba(0,0,0,.1);
    // ${props => props.valid ? null : 'border: 1px solid tomato'};
    margin: 5px 0 20px;
    background: #ffffff;
    font-family: ${typography.body};
    color: rgba(0,0,0,.8);
    outline: ${color.primary}
    font-size: 14px;
`

export const Select = styled.select`
    width: 100%;
    padding: 15px 5px;
    border: none;
    border-radius: 5px;
    border: 1px solid rgba(0,0,0,.1);
    ${props => props.valid ? null : 'border: 1px solid tomato'};
    margin: 10px 0 30px;
    background: #ffffff;
    font-family: ${typography.body};
    color: ${color.primary};
    outline: ${color.primary};
    font-size: 14px;
`

export const Textarea = styled.textarea`
    width: 100%;
    border: none;
    border-radius: 5px;
    border: none;
    margin: 10px 0 30px;
    background: ${color.backgroundGreenLight};;
    font-family: ${typography.body};
    color: #000;
    font-size: 14px;
    padding: 10px;
    height: 100px;

    &::placeholder {
        color: #000
        font-weight: 700;
    }
`
