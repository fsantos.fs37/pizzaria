import styled from "styled-components";
import { color } from "./colors";

export const OrderInfoContainer = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    flex-wrap: wrap;
    min-width: 120px;
    padding: 5px;
    margin-bottom: 20px;
    padding: 10px 5px;
    background: ${color.backgroundGreenLight};
    border-radius: 5px;
    padding: 10px;
    margin: 20px 0 0;

`

export const OrderInfoTitle = styled.p`
    font-size: 17px;
    font-weight: 600;
    color: rgba(0,0,0,.8);
    font-family: 'Lekton', sans-serif;
`

export const OrderListContainer = styled.div`
    display: flex;
    width: 100%;
    max-width: 330px;
    justify-content: space-between;
`

export const OrderListItem = styled.li`
    list-style: none;
    height: 35px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    font-size: 14px;
    font-weight: 600;
    font-family: 'Lekton', sans-serif;
    
    span {
        padding: 0;
    }

`