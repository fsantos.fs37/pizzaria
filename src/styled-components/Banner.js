import styled from 'styled-components';

export const BannerImg = styled.img`
    max-width: 500px;
    width: 100%;
    margin: auto auto 10px;
`
