import { put, takeEvery } from 'redux-saga/effects';
import { ORDER_SUMITTED, NEW_ORDER_SUMITTED, NEW_RECEIVED_ID, RECEIVED_ID, STATUS_UPDATE, NEW_STATUS_UPDATE } from '../actions/actions';

function* orderSubmitted(action) {
    const { payload } = action;
    yield put({ type: ORDER_SUMITTED, payload });
}

export function* watchOrderSubmitted() {
    yield takeEvery(NEW_ORDER_SUMITTED, orderSubmitted);
}

function* receivedOrderId(action) {
    const { payload } = action;
    yield put({ type: RECEIVED_ID, payload });
}

export function* watchReceivedOrderId() {
    yield takeEvery(NEW_RECEIVED_ID, receivedOrderId);
}

function* orderStatusChanged(action) {
    const { payload } = action;
    yield put({ type: STATUS_UPDATE, payload });
}

export function* watchOrderStatusChanged() {
    yield takeEvery(NEW_STATUS_UPDATE, orderStatusChanged);
}
