import { put, takeEvery } from 'redux-saga/effects';
import { NEW_CHECK_FOR_USER, USER_IS_LOGGED, USER_NOT_LOGGED } from '../actions/actions';

import { auth, database } from '../../firebase/firebase'

export function* checkForUser() {
    let user;
    yield auth.getRedirectResult();
    if (auth.currentUser) {
        const { displayName, email, photoURL, uid } = auth.currentUser;
        user = { displayName, email, photoURL, uid };
        const snapShot = yield database.collection('users').doc(uid).get();
        if (snapShot.data()) {
            user = { ...user, adress: snapShot.data().adress }
        }
        yield put({ type: USER_IS_LOGGED, payload: user })
    }
    // else {
    //     yield put({ type: USER_NOT_LOGGED, payload: {} })
    // }
}

export function* watchCheckForUser() {
    yield takeEvery(NEW_CHECK_FOR_USER, checkForUser)
}
