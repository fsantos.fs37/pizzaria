import { put, takeEvery } from 'redux-saga/effects';
import {
    UPDATE_ADRESS, NEW_UPDAtE_ADRESS,
    UPDATE_PAYMENT_METHOD,
    NEW_UPDATE_PAYMENT_METHOD,
    NEW_UPDAtE_USER_CONTACT,
    UPDATE_USER_CONTACT
} from '../actions/actions';

import { database } from '../../firebase/firebase';

function* updateAdress(action) {
    const { payload } = action;
    yield put({ type: UPDATE_ADRESS, payload })
}

export function* watchUpdateAdress() {
    yield takeEvery(NEW_UPDAtE_ADRESS, updateAdress)
}

function* updatePaymentMethod(action) {
    const { payload } = action;
    yield put({ type: UPDATE_PAYMENT_METHOD, payload })
}

export function* watchUpdatePaymentMethod() {
    yield takeEvery(NEW_UPDATE_PAYMENT_METHOD, updatePaymentMethod)
}

function* updateUserContact(action) {
    const { payload } = action;
    yield database.collection('users').doc(payload.user.uid).update({
        adress: payload.adress
    });
    alert('Dados de contato atualizados com sucesso')
    yield put({ type: UPDATE_USER_CONTACT, payload });
    window.location.pathname = '/'

}

export function* watchUpdateUserContact() {
    yield takeEvery(NEW_UPDAtE_USER_CONTACT, updateUserContact);
}

