import { createStore, applyMiddleware, combineReducers } from 'redux'
import createSagaMiddleware from 'redux-saga'
import rootSaga from './sagas/sagas'
import rootReducer from './reducers/reducer'
import cartReducer from './reducers/cart_reducer'
import adressAndCartReducer from './reducers/adressAndPaymentReducer'
import orderReducer from './reducers/orderReducer'

const sagaMiddleware = createSagaMiddleware()
const store = createStore(
    combineReducers({ rootReducer, cartReducer, adressAndCartReducer, orderReducer }),
    applyMiddleware(sagaMiddleware)
)
sagaMiddleware.run(rootSaga)

export default store