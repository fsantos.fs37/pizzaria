// Estas funcões lambda são usadas para padronizar o acesso ao estado da aplicação

export const state = state => {
    return { state: state }
};

export const user = state => {
    return { user: state.rootReducer.user }
};

export const pizzas = state => {
    return { pizzas: state.rootReducer.produtos.pizzas }
};

export const produtos = state => {
    return { produtos: state.rootReducer.produtos }
};

export const price = state => {
    return { price: state.rootReducer.price }
};

export const order = state => {
    return { order: state.rootReducer.order }
};

export const adicionais = state => {
    return { adicionais: state.rootReducer.produtos.adicionais }
};

export const counter = state => {
    return { counter: state.cartReducer.cart.counter }
};

export const parts = state => {
    return { parts: state.cartReducer.cart.parts }
};

export const produto = state => {
    return { produto: state.cartReducer.cart.produto }
};


export const adicionaisCart = state => {
    return { adicionaisCart: state.cartReducer.cart.adicionais }
};

export const bordas = state => {
    return { bordas: state.rootReducer.produtos.bordas }
};

export const bordasCart = state => {
    return { bordasCart: state.cartReducer.cart.bordas }
};

export const cart = state => {
    return { cart: state.cartReducer.cart }
};

export const partsOptions = state => {
    return { partsOptions: state.cartReducer.cart.partsOptions }
}

export const adress = state => {
    return { adress: state.adressAndCartReducer.adress }
}

export const payment = state => {
    return { payment: state.adressAndCartReducer.payment }
}

export const orderReducer = state => {
    return { orderReducer: state.orderReducer }
}

export const orderId = state => {
    return {
        orderId: state.orderReducer.orderId
    }
}

export const orderStatus = state => {
    return { orderStatus: state.orderReducer.status }
}

export const observacao = state => {
    return { observacao: state.cartReducer.cart.observacao }
}

export const horario = state => {
    return { horario: state.rootReducer.horario };
}

export const totalOrder = state => {
    return { totalOrder: state.rootReducer.totalOrder };
}

