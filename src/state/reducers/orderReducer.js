import { ORDER_SUMITTED, STATUS_UPDATE, RECEIVED_ID } from '../actions/actions';

const initialState = {
    order: {},
    status: 'A confirmar',
    orderId: ''
}

export default function orderReducer(state = initialState, action) {
    switch (action.type) {
        case ORDER_SUMITTED:
            return state = { ...state, order: action.payload };
        case STATUS_UPDATE:
            return state = { ...state, status: action.payload };
        case RECEIVED_ID:
            return state = { ...state, orderId: action.payload };
        default:
            return state;
    }
}
