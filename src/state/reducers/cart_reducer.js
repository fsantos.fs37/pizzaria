import { actions, NEW_STAGE_ORDER } from '../actions/actions';

const initialState = {
    cart: {
        produto: {},
        adicionais: [],
        counter: 1,
        parts: 1,
        partsOptions: [],
        bordas: {},
        observacao: '',
    },
    stageCart: {}
}

export default function cartReducer(state = initialState, action) {
    switch (action.type) {
        case actions.INCREMENT_COUNTER:
            return state = { ...state, cart: { ...state.cart, counter: state.cart.counter + 1 } };
        case actions.DECREMENT_COUNTER:
            return state = { ...state, cart: { ...state.cart, counter: state.cart.counter >= 2 ? state.cart.counter - 1 : parseInt(state.cart.counter, 10) } };
        case actions.CHANGE_COUNTER:
            return state = { ...state, cart: { ...state.cart, counter: action.payload } };
        case actions.CHANGE_PARTS:
            return state = { ...state, cart: { ...state.cart, parts: action.payload } };
        case actions.CHANGE_ADICIONAIS:
            return state = { ...state, cart: { ...state.cart, adicionais: action.payload } };
        case actions.CHANGE_PRODUCT:
            return state = {
                ...state, cart: { ...state.cart, produto: action.payload }
            };
        case actions.CURRENT_CART_OPTIONS:
            return state = { ...state, cart: { ...action.payload } };
        case actions.UPDATE_PARTS_OPTIONS:
            return state = { ...state, cart: { ...state.cart, partsOptions: action.payload } };
        case actions.UPDATE_BORDAS_OPTIONS:
            return state = { ...state, cart: { ...state.cart, bordas: action.payload } };
        case actions.UPDATE_OBSERVACAO:
            return state = { ...state, cart: { ...state.cart, observacao: action.payload } };
        case NEW_STAGE_ORDER:
            return state = { ...state, stageCart: { ...state.cart } };
        case actions.RESET_CART_REDUCER:
            return state = {
                ...state, cart: {
                    produto: {},
                    adicionais: [],
                    counter: 1,
                    parts: 1,
                    partsOptions: [],
                    bordas: {},
                    observacao: ''
                }
            };
        default:
            return state;
    }
}
