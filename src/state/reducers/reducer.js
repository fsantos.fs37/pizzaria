import { actions, NEW_HORARIO, GET_ORDER, GET_PROMO_PRODUCTS_SUCCESS } from '../actions/actions';

const initialState = {
    user: {},
    pizzas: [],
    produtos: {
        pizzas: [],
        esfihas: [],
        adicionais: {},
        promoProducts: [],
    },
    price: 0,
    order: [],
    totalOrder: [],
    horario: {
        inicio: 0,
        final: 0,
    },
}

export default function rootReducer(state = initialState, action) {
    switch (action.type) {
        case actions.FETCH_PIZZAS_SUCCESS:
            return state = {
                ...state, produtos: action.payload
            }
        case actions.FETCH_ESFIHAS_SUCCESS:
            return state = {
                ...state, produtos: { ...state.produtos, esfihas: action.payload }
            }
        case actions.USER_IS_LOGGED:
            return state = {
                ...state, user: action.payload
            }
        case actions.USER_NOT_LOGGED:
            return state = {
                ...state, user: action.payload
            }
        case actions.UPDATE_PRICE:
            return state = {
                ...state, price: state.order.map(item => parseFloat(item.totalValue)).reduce((prev, next) => prev + next, 0)
            }
        case actions.UPDATE_ORDER:
            return state = {
                ...state, order: (_ => {
                    const newState = state.order.filter(item => item.id !== action.payload.id);
                    newState.push(action.payload);
                    return newState;
                })()
            }
        case actions.REMOVE_ORDER:
            return state = {
                ...state, order: state.order.filter(item => item.id !== action.payload.id)
            }
        case NEW_HORARIO:
            return state = {
                ...state, horario: action.payload
            }
        case GET_ORDER:
            return state = {
                ...state, totalOrder: action.payload
            }
        case GET_PROMO_PRODUCTS_SUCCESS:
            return state = {
                ...state, produtos: { ...state.produtos, promoProducts: action.payload }
            }
        default:
            return state;
    }
}
