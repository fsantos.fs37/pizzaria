import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { cart } from '../state/mapStates/map_states';

import { StyledSection, SectionTitle } from '../styled-components/section';
import EscPartes from '../containers/EscPartes';

import Accordion from '../containers/Accordion';
import { AccordionButton, AccordionBody, AccordionPane } from '../styled-components/accordion';
import { StyledButton } from '../styled-components/buttons';

import Observacao from '../containers/Observacao';

class PartsScreen extends Component {
    render() {
        return (
            <StyledSection>
                <SectionTitle>Selecionar Partes</SectionTitle>
                <div style={{ marginTop: 50 }}>
                    <Accordion>
                        <AccordionPane>
                            <AccordionButton>
                                <StyledButton>Partes</StyledButton>
                            </AccordionButton>
                            <AccordionBody>
                                <EscPartes />
                            </AccordionBody>
                        </AccordionPane>
                        {/* <AccordionPane>
                            <AccordionButton>
                                <StyledButton>Adicionais</StyledButton>
                            </AccordionButton>
                            <AccordionBody>
                                <Adicionais />
                            </AccordionBody>
                        </AccordionPane> */}
                        {/* <AccordionPane>
                            <AccordionButton>
                                <StyledButton>Borda</StyledButton>
                            </AccordionButton>
                            <AccordionBody>
                                <BordasOptions />
                            </AccordionBody>
                        </AccordionPane> */}
                    </Accordion>
                    {/* <Observacao /> */}
                </div>
                <Link to='/teste'>
                    <StyledButton>Próximo</StyledButton>
                </Link>
            </StyledSection>
        )
    }
}

export default connect(cart)(PartsScreen);
