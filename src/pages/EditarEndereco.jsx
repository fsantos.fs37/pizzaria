import React, { Component } from 'react';
import { connect } from 'react-redux';

import { StyledSection } from '../styled-components/section';

import EditarEnderecoForm from '../containers/EditarEnderecoForm';

class EditarEndereco extends Component {
    render() {
        return (
            <StyledSection>
                <EditarEnderecoForm />
            </StyledSection>
        )
    }
}

export default connect()(EditarEndereco);