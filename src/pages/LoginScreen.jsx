import React, { Component } from "react";
import { Link } from "react-router-dom";
import { StyledSection, SectionTitle } from "../styled-components/section";
import { LoginContainer } from "../styled-components/login";
import { Form, Label, Input } from "../styled-components/form";
import { StyledButton } from "../styled-components/buttons";

import { signInWithEmail, createWithEmail } from "../firebase/firebase";

import { CreateWithEmailButton } from "../containers/AuthButtons";

class LoginScreen extends Component {
  state = {
    user: {
      name: "",
      email: "",
      password: ""
    },
    login: false
  };

  toggleLogin = _ => {
    let { login } = this.state;
    login = !login;
    this.setState({ login });
  };

  handleChange = event => {
    const { name, value } = event.target;
    this.setState({
      user: { ...this.state.user, [`${name}`]: value }
    });
  };

  submitHandler = event => {
    event.preventDefault();
    this.state.login
      ? signInWithEmail({ ...this.state.user })
      : createWithEmail({ ...this.state.user });
  };

  render() {
    return (
      <StyledSection>
        <SectionTitle>Login</SectionTitle>
        <LoginContainer>
          <Form onSubmit={this.submitHandler}>
            {!this.state.login ? (
              <Label htmlfor="name">
                Nome
                <Input
                  onChange={this.handleChange}
                  value={this.state.user.name}
                  type="text"
                  name="name"
                  required
                />
              </Label>
            ) : null}

            <Label htmlfor="email">
              Email
              <Input
                onChange={this.handleChange}
                value={this.state.user.email}
                type="email"
                name="email"
                required
              />
            </Label>
            <Label htmlfor="password">
              Senha
              <Input
                onChange={this.handleChange}
                value={this.state.user.password}
                type="password"
                name="password"
                required
                current-password
              />
            </Label>
            {!this.state.login ? (
              <CreateWithEmailButton form={this.state} />
            ) : (
              <StyledButton type="none" onClick={this.submitHandler} unfilled>
                Fazer login
              </StyledButton>
            )}
          </Form>
          {!this.state.login ? (
            <StyledButton type="none" onClick={this.toggleLogin} unFilled black>
              Fazer login
            </StyledButton>
          ) : (
            <StyledButton type="none" onClick={this.toggleLogin} unFilled black>
              Criar conta com email
            </StyledButton>
          )}
          <Link to="/reset">
            <StyledButton
              type="none"
              // noMargin
              noPadding
              unFilled
              black
            >
              Esqueci minha senha
            </StyledButton>
          </Link>
        </LoginContainer>
      </StyledSection>
    );
  }
}

export default LoginScreen;
