import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { cart, adicionais } from '../state/mapStates/map_states';
import { actions } from '../state/actions/actions';

import { StyledSection, SectionTitle } from '../styled-components/section';

import Accordion from '../containers/Accordion';
import { AccordionButton, AccordionBody, AccordionPane } from '../styled-components/accordion';
import { StyledButton } from '../styled-components/buttons';

import AddToCartButton from '../containers/AddToCartButton';
// import BordasOptions from '../containers/BordasOptions';

import { AdicionaisLabel, AdicionaisPrice, AdicionaisInput } from '../styled-components/adicionais';

import Observacao from '../containers/Observacao';

import accounting from '../utils/formatMoney';

class PartsScreen extends Component {

    state = {
        name1: {
            values: []
        },
        name2: {
            values: []
        },
        name3: {
            values: []
        },
    }

    componentDidMount() {
        this.props.changedParts([]);
    }

    handleClick = (e, name) => {
        const value = e.currentTarget.querySelector('input').value;
        const values = [...new Set(this.state[name].values)];
        if (values.includes(value)) {
            const filteredValues = values.filter(item => item !== value);
            this.setState({
                [name]: { values: filteredValues }
            }, () => this.updatePartsOptionsReducer()
            )
            return console.log('exists')
        }
        if (this.state[name].values.length === this.state.limit) return;
        values.push(value)
        this.setState({
            [name]: { ...this.state[name], values: values }
        }, () => this.updatePartsOptionsReducer());

    }

    updatePartsOptionsReducer = _ => {
        this.props.updateParts(this.decorateParts())
    };

    decorateParts = _ => {
        const { cart: { partsOptions } } = this.props.cart;
        const { adicionais } = this.props.adicionais

        const newParts = partsOptions.map((part, i) => {
            const obj = this.state['name' + (i + 1)].values.map(id => {
                return adicionais.filter(item => item.id === id)[0]
            })
            console.log(obj);

            part.adicionais = obj;
            return part;
        });
        return newParts
    }

    handleChange = event => {

    }

    render() {

        console.log(this.props);

        const { cart: { partsOptions } } = this.props.cart
        const { adicionais } = this.props.adicionais

        const escolherAdicionalParaParte = partsOptions
            .map((parte, i) => {
                let name = 'name' + (i + 1);
                console.log(this.state[name], name)
                return (
                    <AccordionPane key={parte.id}>
                        <AccordionButton>
                            <StyledButton>{parte.name}</StyledButton>
                        </AccordionButton>
                        <AccordionBody>
                            {
                                adicionais.map((item, index) => (<AdicionaisLabel
                                    htmlFor={item.name}
                                    value={index + 1}
                                    onClick={(e) => {
                                        this.handleClick(e, name);
                                        this.decorateParts(parte.id)
                                    }}
                                    marked={this.state[name].values.includes(item.id)}
                                    filled="true"
                                    full
                                    key={item.name}>
                                    <div>{item.name}</div>
                                    <AdicionaisPrice>{accounting.formatMoney(item.value)}</AdicionaisPrice>
                                    <AdicionaisInput name={item.name} type="radio" value={item.id} onChange={this.handleChange} />
                                </AdicionaisLabel>))
                            }
                        </AccordionBody>
                    </AccordionPane>
                )
            })

        return (
            <StyledSection>
                <SectionTitle>Selecionar Partes</SectionTitle>
                <div style={{ marginTop: 50 }}>
                    <Accordion>
                        {escolherAdicionalParaParte}
                    </Accordion>
                    <Observacao />
                </div>
                <AddToCartButton />
            </StyledSection>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        changedParts: (payload) => dispatch({ type: actions.UPDATE_CHANGE_ADICIONAIS, payload }),
        updateParts: (payload) => dispatch({ type: actions.NEW_UPDATE_PARTS_OPTIONS, payload }),
    }
};

const mapStateToProps = (state) => {
    return {
        cart: cart(state),
        adicionais: adicionais(state)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PartsScreen);
