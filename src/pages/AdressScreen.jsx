import React, { Component } from 'react';
import { connect } from 'react-redux';

import { StyledSection, SectionTitle } from '../styled-components/section';

import AdressForm from '../containers/AdressForm';

class AdressScreen extends Component {
    render() {
        return (
            <StyledSection>
                <SectionTitle>Entrega</SectionTitle>
                <AdressForm />
            </StyledSection>
        )
    }
}

export default connect()(AdressScreen);
