import React, { Component } from 'react';

import LoadingPage from './LoadingPage';

class ErrorPage extends Component {
    render() {
        return <LoadingPage>Error 404</LoadingPage>
    }
}

export default ErrorPage;
