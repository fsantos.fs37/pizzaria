import React, { Component } from "react";

import {
  StyledSection,
  SectionTitle,
  SectionText
} from "../styled-components/section";
import { LoginContainer } from "../styled-components/login";
import { Form, Label, Input } from "../styled-components/form";
import { StyledButton } from "../styled-components/buttons";

import { auth } from "../firebase/firebase";

class ResetarSenha extends Component {
  state = {
    email: "",
    emailSent: false
  };

  handleChange = event => {
    const { name, value } = event.target;
    this.setState({
      [name]: value
    });
  };

  submitHandler = event => {
    event.preventDefault();
    const { email } = this.state;
    if (email) {
      auth.sendPasswordResetEmail(email).then(_ => {
        this.setState({ emailSent: true });
      });
    }
  };

  render() {
    return (
      <StyledSection>
        <SectionTitle>Redefinir senha</SectionTitle>
        {!this.state.emailSent ? (
          <LoginContainer>
            <Form onSubmit={this.submitHandler}>
              <Label htmlfor="email">
                Insira o e-mail usado no cadastro abaixo:
                <Input
                  onChange={this.handleChange}
                  value={this.state.email}
                  type="email"
                  name="email"
                  required
                />
              </Label>
              <StyledButton type="none" onClick={this.submitHandler} unfilled>
                Enviar link de redefinição
              </StyledButton>
            </Form>
          </LoginContainer>
        ) : (
          <SectionText>
            Um link de redefinição foi enviado para o email informado. Verifique
            a caixa de entrada de {this.state.email}
          </SectionText>
        )}
      </StyledSection>
    );
  }
}

export default ResetarSenha;
