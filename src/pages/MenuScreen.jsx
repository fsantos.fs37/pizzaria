import React from 'react';
import { Link } from 'react-router-dom';

import { StyledSection, SectionTitle } from '../styled-components/section';
import { MenuContainer, MenuItem } from '../styled-components/menu';
import { AccordionButton, AccordionBody, AccordionPane } from '../styled-components/accordion';

import Accordion from '../containers/Accordion';
import MenuButton from '../containers/MenuButton';

const MenuScreen = _ => {
    return (
        <StyledSection >
            <SectionTitle>Menu</SectionTitle>
            <MenuContainer>
                <Accordion>
                    <AccordionPane className='accordion-child' >
                        <AccordionButton >
                            <MenuButton>Pizzas Broto</MenuButton>
                        </AccordionButton>
                        <AccordionBody >
                            <Link to='/produto/pizzas/broto/salgadas'>
                                <MenuItem isChild>Salgadas</MenuItem>
                            </Link>
                            <Link to='/produto/pizzas/broto/doces'>
                                <MenuItem isChild>Doces</MenuItem>
                            </Link>
                        </AccordionBody>
                    </AccordionPane>

                    <AccordionPane className='accordion-child' >
                        <AccordionButton >
                            <MenuButton>Pizzas Grandes</MenuButton>
                        </AccordionButton>
                        <AccordionBody >
                            <Link to='/produto/pizzas/grande/salgadas'>
                                <MenuItem isChild>Salgadas</MenuItem>
                            </Link>
                            <Link to='/produto/pizzas/grande/doces'>
                                <MenuItem isChild>Doces</MenuItem>
                            </Link>
                        </AccordionBody>
                    </AccordionPane>
                    <AccordionPane className='accordion-child' >
                        <AccordionButton >
                            <MenuButton>Esfihas</MenuButton>
                        </AccordionButton>
                        <AccordionBody >
                            <Link to='/produto/esfihas/unset/salgadas'>
                                <MenuItem isChild>Salgadas</MenuItem>
                            </Link>
                            <Link to='/produto/esfihas/unset/doces'>
                                <MenuItem isChild>Doces</MenuItem>
                            </Link>
                            <Link to='/produto/esfihas/unset/especiais'>
                                <MenuItem isChild>Especiais</MenuItem>
                            </Link>
                        </AccordionBody>
                    </AccordionPane>
                    <AccordionPane className='accordion-child' >
                        <AccordionButton >
                            <MenuButton>Bebidas</MenuButton>
                        </AccordionButton>
                        <AccordionBody >
                            <Link to='/produto/bebidas/unset/refrigerantes'>
                                <MenuItem isChild>Refrigerantes</MenuItem>
                            </Link>
                            <Link to='/produto/bebidas/unset/cervejas'>
                                <MenuItem isChild>Cervejas</MenuItem>
                            </Link>
                        </AccordionBody>
                    </AccordionPane>
                    <AccordionPane className='accordion-child' >
                        <AccordionButton >
                            <MenuButton>Combo de Esfihas</MenuButton>
                        </AccordionButton>
                        <AccordionBody >
                            <Link to='/produto/esfihas/unset/combo'>
                                <MenuItem isChild>Esfihas</MenuItem>
                            </Link>
                        </AccordionBody>
                    </AccordionPane>
                    {/* <AccordionPane className='accordion-child' >
                        <AccordionButton >
                            <MenuButton>Combos</MenuButton>
                        </AccordionButton>
                        <AccordionBody >
                            <Link to='/produto/pizzas/unset/pizzasCombo'>
                                <MenuItem isChild>Combos</MenuItem>
                            </Link>
                        </AccordionBody>
                    </AccordionPane> */}
                </Accordion>
            </MenuContainer>
        </StyledSection>
    )
}

export default MenuScreen;
