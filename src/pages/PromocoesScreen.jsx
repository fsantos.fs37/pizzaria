import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { produtos } from '../state/mapStates/map_states';

import { auth } from '../firebase/firebase';

import { StyledSection, SectionTitle } from '../styled-components/section';
import { StyledButton } from '../styled-components/buttons';
import { InformacoesSection, InformacoesContainer, InformacoesTitle, InformacoesSubtitle } from '../styled-components/informacoes';

import UserPedidosCounter from '../containers/UserPedidosCounter';

import { LoginButton, LinkToLoginWithEmailButton, LogoutButton } from '../containers/AuthButtons'

const InformacoesScreen = props => {
    let combo;
    const { promoProducts } = props.produtos;
    if (promoProducts) {
        combo = promoProducts
    }
    console.log(promoProducts)
    return (
        <StyledSection>
            <SectionTitle>Promoções</SectionTitle>
            <InformacoesSection>
                {!auth.currentUser &&
                    <InformacoesContainer>
                        <InformacoesTitle center bottom>Faça parte do nosso programa de fidelidade!</InformacoesTitle>
                        <InformacoesSubtitle center>
                            A cada pedido acima de 25 reais você ganha um selo especial!
                        </InformacoesSubtitle>
                        <InformacoesSubtitle center>
                            Crie uma conta no aplicativo e participe!
                        </InformacoesSubtitle>
                    </InformacoesContainer> ||
                    <InformacoesContainer>
                        <InformacoesTitle center bottom>Você já faz parte do nosso programa de fidelidade!</InformacoesTitle>
                        <InformacoesSubtitle center>
                            A cada pedido acima de 25 reais você ganha um selo especial!
                        </InformacoesSubtitle>
                        <InformacoesSubtitle center>
                            {promoProducts && promoProducts[0] ? `Junte 10 selos e ganhe uma pizza ${promoProducts[0].name} ou uma pizza ${promoProducts[1].name} grátis!` : null}
                        </InformacoesSubtitle>
                        <UserPedidosCounter />
                    </InformacoesContainer>}
                <LoginButton />
                <LinkToLoginWithEmailButton />
                <LogoutButton />

            </InformacoesSection>
        </StyledSection>
    )
};

export default connect(produtos)(InformacoesScreen)

