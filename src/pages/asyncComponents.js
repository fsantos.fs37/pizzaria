import React from "react";
import Loadable from "react-loadable";

import LoadingPage from "./LoadingPage";

function importAsync(path, loadscreen = <LoadingPage />) {
  return Loadable({
    loader: () => import(`${path}`),
    loading: () => loadscreen,
    delay: 300
  });
}

export const MenuScreen = importAsync("./MenuScreen");

export const ProductsListScreen = importAsync("./ProductsListScreen");

export const ProductScreen = importAsync("./ProductScreen");

export const PedidoScreen = importAsync("./PedidoScreen");

export const CarrinhoScreen = importAsync("./CarrinhoScreen");

export const PartsScreen = importAsync("./PartsScreen");

export const AdressScreen = importAsync("./AdressScreen");

export const Pagamento = importAsync("./Pagamento");

export const ResumeScreen = importAsync("./ResumeScreen");

export const UserScreen = importAsync("./UserScreen");

export const EditarEndereco = importAsync("./EditarEndereco");

export const InformacoesScreen = importAsync("./InformacoesScreen");

export const LoginScreen = importAsync("./LoginScreen");

export const PromocoesScreen = importAsync("./PromocoesScreen");

export const ResetarSenha = importAsync("./ResetarSenha");
