import React, { Component } from 'react';
import { connect } from 'react-redux';

import { adicionais, adicionaisCart } from '../state/mapStates/map_states'
import { actions } from '../state/actions/actions'

import { AccordionButton, AccordionBody, AccordionPane } from '../styled-components/accordion';
import { AdicionaisButton } from '../styled-components/adicionais';

import Accordion from './Accordion';

import AdicionaisOptions from './AdicionaisOptions';

class Adicionais extends Component {
    render() {
        return (
            <Accordion>
                <AccordionPane className='accordion-child' >
                    <AccordionButton >
                        <AdicionaisButton center filled full onClick={this.removeFromCart}>Adicionais</AdicionaisButton>
                    </AccordionButton>
                    <AccordionBody >
                        <AdicionaisOptions />
                    </AccordionBody>
                </AccordionPane>
            </Accordion>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        updateAdicionais: payload => dispatch({ type: actions.UPDATE_CHANGE_ADICIONAIS, payload })
    }
}

const mapStateToProps = state => {
    return {
        adicionais: adicionais(state),
        adicionaisCart: adicionaisCart(state)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Adicionais);
