import React from 'react';
import { LoadingScreenContainer, LoadingMessage, Logo } from '../styled-components/loadingScreen';
import logo from '../assets/logo.png'

export const LoadingScreen = props => {
    return (
        <LoadingScreenContainer showLoadingScreen={props.show}>
            <Logo src={logo}/>
            <LoadingMessage>Carregando preferências...</LoadingMessage>
        </LoadingScreenContainer>
    )
}
