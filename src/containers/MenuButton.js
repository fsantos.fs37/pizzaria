import React from 'react';
import { MenuItem } from '../styled-components/menu';

const MenuButton = props => {
    return (
        <MenuItem>
            {props.children}
            <i className="material-icons">keyboard_arrow_down</i>
        </MenuItem>
    )
}

export default MenuButton;