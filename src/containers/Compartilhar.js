import React, { Component } from 'react';

export class ShareButton extends Component {

    state = {
        support: true,
    }

    componentDidMount() {
        if (!navigator.share) return this.setState({ support: false })
    }

    share = event => {
        if (navigator.share) {
            event.target.disabled = true;
            navigator.share({
                title: 'Aplicativo',
                text: 'Conheça o novo aplicativo para pedir pizza!',
                url: window.location.href,
            })
                .then(() => event.target.removeAttribute('disabled'))
                .catch((error) => console.log('Error sharing', error));
        }
        console.log('Error sharing')
    }

    render() {

        return (
            this.state.support && <div className='share-button' onClick={this.share}>{this.props.children}</div>
        )
    }
}

