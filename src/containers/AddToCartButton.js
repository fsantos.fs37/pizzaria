import React, { Component } from 'react';
import { connect } from 'react-redux';

import { cart, horario, order } from '../state/mapStates/map_states';
import { actions } from '../state/actions/actions';

import { ConfirmOrderButton } from '../styled-components/buttons';

class AddToCartButton extends Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    formatOrder() {
        let { cart: { produto, counter, adicionais, parts, partsOptions, bordas, observacao } } = this.props.cart;

        const CartOrder = this.props.order.order;

        const filterOrderPromo = CartOrder.map(item => item.tipo === 'promocao');

        if (filterOrderPromo.length >= 1 && produto.tipo === 'promocao') {
            return alert('Você já selecionou um item da promoção!')
        }

        let comAdicionais = adicionais.map(item => parseFloat(item.value)).reduce((prev, next) => prev + next, 0);
        const comBorda = bordas.value ? parseFloat(bordas.value) : 0;

        const status = false;
        if (partsOptions.length > 0) {
            if (partsOptions.map(part => part.adicionais).length > 0) {
                comAdicionais = partsOptions.map(temp => temp.adicionais).reduce((prev, next) => prev.concat(next)).map(item => item.value).reduce((prev, next) => Number.parseInt(prev) + Number.parseInt(next))
            }
        }

        if (parts > 1) {
            counter = 1
        }

        const totalValue = (produto.value * counter) + comAdicionais + comBorda;

        const order = {
            ...produto,
            totalValue,
            counter,
            adicionais,
            parts,
            partsOptions,
            bordas,
            status,
            observacao
        };
        return order;
    }

    handleClick() {
        const D = new Date();
        const { horario: { horario } } = this.props;
        const { cart: { produto } } = this.props.cart;
        // if (D.getHours() + 0.1 < horario.inicio) {
        //     return alert('Ainda não estamos abertos! Abrimos às 18h.')
        // }
        if (produto.id) {
            this.props.newOrder(this.formatOrder());
        }
    }

    render() {
        return (
            <ConfirmOrderButton filled full center onClick={this.handleClick}>
                Adicionar ao pedido
            </ConfirmOrderButton>
        )
    }
}

const mapStateToProps = state => {
    return {
        order: order(state),
        cart: cart(state),
        horario: horario(state),
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        newOrder: payload => dispatch({ type: actions.NEW_ORDER, payload }),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(AddToCartButton);
