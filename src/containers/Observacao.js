import React, { Component } from 'react';
import { connect } from 'react-redux';

import { observacao } from '../state/mapStates/map_states';
import { UPDATE_OBSERVACAO } from '../state/actions/actions';

import { Textarea } from '../styled-components/form';

class Observacao extends Component {

    state = {
        value: ''
    }

    changeHandler = event => {
        const { value } = event.target;
        this.setState({ value }, _ => this.props.updateObservacao(this.state.value));
    }

    render() {
        return (
            <Textarea onChange={this.changeHandler} value={this.state.value} placeholder='Observações' />
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        updateObservacao: payload => dispatch({ type: UPDATE_OBSERVACAO, payload })
    }
}

export default connect(observacao, mapDispatchToProps)(Observacao);
