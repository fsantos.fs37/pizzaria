import React, { Component } from 'react';
import { connect } from 'react-redux';

import { cart } from '../state/mapStates/map_states';

import Accordion from '../containers/Accordion';
import { AccordionButton, AccordionBody, AccordionPane } from '../styled-components/accordion';
import { StyledButton } from '../styled-components/buttons';

import Adicionais from './AdicionaisOptions';
import BordasOptions from './BordasOptions';
// import AddToCartButton from '../containers/AddToCartButton';

class ProductScreenOptions extends Component {
    render() {
        return (
            <div style={{marginTop: 20}}>
                <Accordion>
                    <AccordionPane>
                        <AccordionButton>
                            <StyledButton>Adicionais</StyledButton>
                        </AccordionButton>
                        <AccordionBody>
                            <Adicionais />
                        </AccordionBody>
                    </AccordionPane>
                    <AccordionPane>
                        <AccordionButton>
                            <StyledButton>Bordas</StyledButton>
                        </AccordionButton>
                        <AccordionBody>
                            <BordasOptions />
                        </AccordionBody>
                    </AccordionPane>
                </Accordion>
            </div>
        )
    }
}

export default connect(cart)(ProductScreenOptions);
