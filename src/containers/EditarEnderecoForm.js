import React, { Component } from 'react';
import { connect } from 'react-redux';

import { order, adress, user } from '../state/mapStates/map_states';
import { NEW_UPDAtE_ADRESS, NEW_UPDAtE_USER_CONTACT } from '../state/actions/actions';

import { StyledButton } from '../styled-components/buttons';
import { Form, Input, Label, FormTitle } from '../styled-components/form';

// import {auth, data} from '../firebase/firebase'

class EditarEnderecoForm extends Component {

    state = {
        bairro: { value: '', required: true, valid: false },
        cep: { value: '', required: true, valid: false },
        complemento: { value: '', required: false },
        nome: { value: '', required: true, valid: false },
        numero: { value: '', required: true, valid: false },
        ready: false,
        rua: { value: '', required: true, valid: false },
        tel: { value: '', required: true, valid: false }
    }

    componentDidMount() {
        const { adress } = this.props.adress;
        this.setState({ ...adress, ready: false })
    }

    handleSubmit = event => {
        const { user } = this.props.user;
        const data = { user, adress: this.state }
        event.preventDefault();
        this.setState({ ready: true }, () => {
            if (user.uid) {
                this.props.updateUserContact(data);
            }
        })
    }

    handleChange = event => {
        const { name, value } = event.target;
        this.setState({
            [name]: { ...this.state[name], value }
        });
    }

    render() {
        const { nome, tel, rua, numero, complemento, bairro, cep } = this.state;
        return (
            < Form onSubmit={this.handleSubmit} >
                <FormTitle>Sobre você:</FormTitle>
                <Label htmlFor="nome">
                    Nome completo
                        <Input onChange={this.handleChange} value={nome.value} valid={nome.valid === true} placeholder="ex. Maria José" name="nome" type="text" autoFocus required />
                </Label>
                <Label htmlFor="tel">
                    Telefone
                        <Input onChange={this.handleChange} value={tel.value} valid={tel.valid === true} placeholder="ex. 55 5 5555 5555" name="tel" type="tel" required />
                </Label>
                <FormTitle>Endereço:</FormTitle>
                <Label htmlFor="rua">
                    Rua
                        <Input onChange={this.handleChange} value={rua.value} valid={rua.valid === true} placeholder="ex. Rua Arco Verde" name="rua" type="text" required />
                </Label>
                <Label htmlFor="numero">
                    Número
                        <Input onChange={this.handleChange} value={numero.value} valid={numero.valid === true} placeholder="ex. 42" name="numero" type="number" required />
                </Label>
                <Label htmlFor="complemento">
                    Complemento
                        <Input onChange={this.handleChange} value={complemento.value} valid={true} placeholder="ex. Apt. 123" name="complemento" type="text" />
                </Label>
                <Label htmlFor="bairro">
                    Bairro
                        <Input onChange={this.handleChange} value={bairro.value} valid={bairro.valid === true} placeholder="ex. Bairro das Flores" name="bairro" type="text" required />
                </Label>
                <Label htmlFor="cep">
                    CEP
                        <Input onChange={this.handleChange} value={cep.value} valid={cep.valid === true} placeholder="ex. 12345-678" name="cep" type="text" required inputMode="numeric" />
                </Label>
                <StyledButton type="submit">Configurar como endereço padrão</StyledButton>
            </Form >
        )
    }
}

const mapStateToProps = state => {
    return {
        order: order(state),
        adress: adress(state),
        user: user(state),
    }
}

const mapDispatchToProps = dispatch => {
    return {
        updateAdress: payload => dispatch({ type: NEW_UPDAtE_ADRESS, payload }),
        updateUserContact: payload => dispatch({ type: NEW_UPDAtE_USER_CONTACT, payload })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditarEnderecoForm);
