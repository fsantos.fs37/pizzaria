import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { order, adress, user } from '../state/mapStates/map_states';
import { NEW_UPDAtE_ADRESS, NEW_UPDAtE_USER_CONTACT } from '../state/actions/actions';

import { StyledButton } from '../styled-components/buttons';
import { Form, Input, Label, FormTitle } from '../styled-components/form';

class AdressForm extends Component {

    state = {
        bairro: { value: "São mIguel", required: true, valid: true },
        cep: { value: "63010-555", required: true, valid: true },
        complemento: { value: "APT 202", required: false },
        nome: { value: "Francisco Santos", required: true, valid: true },
        numero: { value: "34", required: true, valid: true },
        ready: false,
        rua: { value: "Rua Santa Isabel", required: true, valid: true },
        tel: { value: "81141990", required: true, valid: true },
        isPromo: false
    }

    isPromo = false;

    componentDidMount() {
        const { adress } = this.props.adress;
        this.setState({ ...adress, ready: false })
    }

    handleSubmit = event => {
        const { user } = this.props.user;
        const data = { user, adress: this.state }
        event.preventDefault();

        let dataToUpload = { ...this.state };
        delete data.isPromo;

        if (this.isPromo) {
            return this.setState({ isPromo: true }, () => {
                this.props.updateAdress(dataToUpload);
            })
        }
        this.setState({ ready: true }, () => {
            this.props.updateAdress(dataToUpload);
        })
    }

    handleChange = event => {
        const { name, value } = event.target;
        this.setState({
            [name]: { ...this.state[name], value }
        });
    }

    render() {
        const { order } = this.props.order;

        const checkForPromo = order.filter(item => item.tipo === 'promocao');

        if (checkForPromo.length >= 1 && order.length === 1) {
            this.isPromo = true;
        }
        const { ready, nome, tel, rua, numero, complemento, bairro, cep, isPromo } = this.state;
        return (
            ready
            && <Redirect push to="pagamento" />
            || isPromo && <Redirect push to="/resumo" />
            ||
            < Form onSubmit={this.handleSubmit} >
                <FormTitle>Sobre você:</FormTitle>
                <Label htmlFor="nome">
                    Nome completo
                        <Input onChange={this.handleChange} value={nome.value} valid={nome.valid === true} placeholder="ex. Maria José" name="nome" type="text" autoFocus required />
                </Label>
                <Label htmlFor="tel">
                    Telefone
                        <Input onChange={this.handleChange} value={tel.value} valid={tel.valid === true} placeholder="ex. 55 5 5555 5555" name="tel" type="tel" required />
                </Label>
                <FormTitle>Endereço:</FormTitle>
                <Label htmlFor="rua">
                    Rua
                        <Input onChange={this.handleChange} value={rua.value} valid={rua.valid === true} placeholder="ex. Rua Arco Verde" name="rua" type="text" required />
                </Label>
                <Label htmlFor="numero">
                    Número
                        <Input onChange={this.handleChange} value={numero.value} valid={numero.valid === true} placeholder="ex. 42" name="numero" type="number" required />
                </Label>
                <Label htmlFor="complemento">
                    Complemento
                        <Input onChange={this.handleChange} value={complemento.value} valid={true} placeholder="ex. Apt. 123" name="complemento" type="text" />
                </Label>
                <Label htmlFor="bairro">
                    Bairro
                        <Input onChange={this.handleChange} value={bairro.value} valid={bairro.valid === true} placeholder="ex. Bairro das Flores" name="bairro" type="text" required />
                </Label>
                <Label htmlFor="cep">
                    CEP
                        <Input onChange={this.handleChange} value={cep.value} valid={cep.valid === true} placeholder="ex. 12345-678" name="cep" type="text" required inputMode="numeric" />
                </Label>
                <StyledButton type="submit">Confirmar e ir para método de pagamento</StyledButton>
            </Form >
        )
    }
}

const mapStateToProps = state => {
    return {
        order: order(state),
        adress: adress(state),
        user: user(state),
    }
}

const mapDispatchToProps = dispatch => {
    return {
        updateAdress: payload => dispatch({ type: NEW_UPDAtE_ADRESS, payload }),
        updateUserContact: payload => dispatch({ type: NEW_UPDAtE_USER_CONTACT, payload })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdressForm);
