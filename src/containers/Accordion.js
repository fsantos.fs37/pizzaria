import React, { Component } from 'react';

class Accordion extends Component {
    state = {
        active: 0,
    }

    constructor(props) {
        super(props);
        this.className = 'active-' + parseInt(Math.random() * Math.floor(100), 10);
    }

    componentDidMount() {
        const childs = document.querySelector('.accordion-container').children
        Array.from(childs).map((node, index) => {
            node.firstElementChild.setAttribute('value', index + 1);
            return node.firstElementChild.addEventListener('click', this.clickHandler.bind(this));
        })
    }

    clickHandler(e) {
        const active = document.querySelector('[active="true"');
        if (active) {
            active.style.height = '0px';
            active.removeAttribute('active');;
        }
        if (e.currentTarget.nextElementSibling === active) {
            e.currentTarget.nextElementSibling.style.height = '0px';
            return e.currentTarget.nextElementSibling.removeAttribute('active');
        }

        const height = [...e.currentTarget.nextElementSibling.children]
            .map(item => {
                return item.tagName === 'A'
                    ? item.firstElementChild.offsetHeight + 20
                    : item.offsetHeight + 20
            })
            .reduce((prev, next) => prev + next, 0);


        e.currentTarget.nextElementSibling.setAttribute('active', 'true');
        e.currentTarget.nextElementSibling.classList.add(this.className)
        e.currentTarget.nextElementSibling.style.height = height + 'px';
    }

    render() {
        return (
            <div className='accordion-container'>
                {this.props.children}
            </div>
        )
    }
}

export default Accordion;
