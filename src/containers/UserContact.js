import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';

import { user } from '../state/mapStates/authState';

import {
    AdressCardContainer,
    AdressCardTitle,
    AdressCardText
} from '../styled-components/adressCard';

import { StyledButton } from '../styled-components/buttons';

class UserContact extends Component {
    render() {
        const { user } = this.props;
        const { adress } = user;
        return (
            adress
                ? <Link to='/editar-endereco'>
                    <AdressCardContainer>
                        <AdressCardTitle>Dados de contato padrão para entrega (toque para editar)</AdressCardTitle>
                        <AdressCardText><b>Telefone</b> {adress.tel.value}</AdressCardText>
                        <AdressCardText><b>Rua</b> {adress.rua.value}</AdressCardText>
                        <AdressCardText><b>N°</b> {adress.numero.value}</AdressCardText>
                        <AdressCardText><b>Bairro</b> {adress.bairro.value}</AdressCardText>
                        <AdressCardText><b>CEP</b> {adress.cep.value}</AdressCardText>
                        <AdressCardText><b>Complemento</b> {adress.complemento.value}</AdressCardText>
                    </AdressCardContainer>
                </Link> : user.uid ? <Link to='/editar-endereco'><StyledButton>Adicionar dados de contato</StyledButton></Link> : null
        )
    }
}

export default connect(user)(UserContact);
