import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import accounting from '../utils/formatMoney';

import { ProductButtonContainer, ProductButtonTitle, ProductButtonDescription, ProductPriceTag } from '../styled-components/productButton';

class ProductButton extends Component {
    render() {
        const { produto, data } = this.props;
        const price = accounting.formatMoney(data.value);
        const path = `/detalhe/${produto}/${data.id.toLowerCase()}`;
        return (
            <Link to={path}>
                <ProductButtonContainer>
                    <ProductButtonTitle>
                        {data.name}
                        <ProductPriceTag>{price}</ProductPriceTag>
                    </ProductButtonTitle>
                    <ProductButtonDescription>
                        {data.ingredients}
                    </ProductButtonDescription>
                </ProductButtonContainer>
            </Link>
        )
    }
}

export default ProductButton;
