import React, { Component } from 'react';
import { connect } from 'react-redux';

import { parts, cart } from '../state/mapStates/map_states'
import { actions } from '../state/actions/actions'

import { PartOptionContainer, PartOptionLabel, PartOptionInput } from '../styled-components/partes';

class PartsOptions extends Component {
    state = {
        value: 1
    }

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleClick(e) {
        const { value } = e.currentTarget.firstElementChild;
        this.setState({
            value: parseInt(value, 10)
        }, () => {
            this.props.changedParts(parseInt(value, 10))
        })
    }

    handleChange(e) {
    }

    render() {
        const { parts } = this.props.parts
        const { cart } = this.props.cart;
        if (cart.produto.numeroSabores < 2) {
            this.handleClick = null;
        }
        console.log('Parts options', cart.produto)
        return (
            <PartOptionContainer>
                <PartOptionLabel onClick={this.handleClick} checked={parts === 1}>
                    Inteira
                    <PartOptionInput type="radio" value="1" onChange={this.handleChange} />
                </PartOptionLabel>
                <PartOptionLabel onClick={this.handleClick} checked={parts === 2}>
                    Meio-a-meio
                    <PartOptionInput type="radio" value="2" onChange={this.handleChange} />
                </PartOptionLabel>
                {cart.produto.tipo !== 'pizzasCombo' && <PartOptionLabel onClick={this.handleClick} checked={parts === 3}>
                    Três sabores
                    <PartOptionInput type="radio" value="3" onChange={this.handleChange} />
                </PartOptionLabel>}
            </PartOptionContainer>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        changedParts: (payload) => dispatch({ type: actions.UPDATE_CHANGE_PARTS, payload }),
    }
}

const mapStateToProps = (state) => {
    return {
        parts: parts(state),
        cart: cart(state)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PartsOptions);
