import React, { Component } from 'react';
import { connect } from 'react-redux';

import { cart } from '../state/mapStates/map_states';
import { actions } from '../state/actions/actions';

import { ConfirmOrderButton } from '../styled-components/buttons';

class RemoveFromCartButton extends Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    formatOrder() {
        const { produto, counter, adicionais, parts, partsOptions } = this.props.cart;
        const comAdicionais = adicionais.map(item => item.value).reduce((prev, next) => prev + next, 0)
        const order = {
            ...produto,
            value: produto.value * counter + comAdicionais,
            counter,
            adicionais,
            parts,
            partsOptions
        };
        return order;
    }

    handleClick() {
        const { id } = this.props.cart.produto;

        if (id) {
            this.props.removeOrder(this.formatOrder());
        }
    }

    render() {
        return (
            <ConfirmOrderButton white full center onClick={this.handleClick}>
                Remover
       </ConfirmOrderButton>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        removeOrder: payload => dispatch({ type: actions.NEW_REMOVE_ORDER, payload }),
    }
}


export default connect(cart, mapDispatchToProps)(RemoveFromCartButton);
