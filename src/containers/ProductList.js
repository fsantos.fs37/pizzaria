import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { produtos } from '../state/mapStates/map_states'

import { MenuContainer } from '../styled-components/menu';
import ProductButton from './ProductButton';

class ProductList extends Component {
    render() {
        // As informações sobre o produto são extraídas das "props"
        // passadas pelo componente pai 
        const { produto, produtos, tipo, tamanho } = this.props;
        if (!produtos[produto]) return <Redirect to='/404' />;

        // As opções são cruzadas com os produtos disponíveis no cardápio,
        // filtradas de acordo com o tipo e tamanho e passadas para o
        // componente responsável pela apresentação das opções
        const elements = produtos[produto]
            .filter(item => item.tipo === tipo && (item.size === tamanho || item.size === '' || !item.size))
            .map(p => <ProductButton key={p.id} data={p} produto={produto} />);

        return (
            <MenuContainer>
                {elements}
            </MenuContainer >
        )
    }
}

export default connect(produtos)(ProductList);
