import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { auth, database } from '../firebase/firebase';

import { totalOrder } from '../state/mapStates/map_states';

import { StyledButton, OrderButtonContainer } from '../styled-components/buttons';

class UserPedidosCounter extends Component {
    render() {
        const { totalOrder } = this.props;
        const valids = totalOrder.filter(order => order.price > 25).length;
        return (
            auth.currentUser && <OrderButtonContainer transparent>
                <StyledButton>
                    Você possui: <b style={{ fontSize: 16 }}>{`${valids} selos!`}</b>
                </StyledButton>
                {valids >= 10 &&
                    <Link to='/produto/promoProducts/unset/promocao'>
                        <StyledButton>
                            Realizar Pedido!
                        </StyledButton>
                    </Link>
                }
            </OrderButtonContainer> || null
        )
    }
}

export default connect(totalOrder)(UserPedidosCounter);
