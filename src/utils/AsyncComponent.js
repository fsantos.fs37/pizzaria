import React from 'react';
import Loadable from 'react-loadable';

export const AsyncComponent = (path, loadComponent) => Loadable({
  loader: () => import(path),
  loading: () => loadComponent ? loadComponent : <div>Loading</div>,
  delay: 300, 
});
